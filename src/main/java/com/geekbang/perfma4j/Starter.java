package com.geekbang.perfma4j;

/**
 * 仿JMeter性能测试工具
 */
public class Starter
{
    /**
     * 测试地址
     */
    public static final String URL = "https://www.baidu.com";
    /**
     * 线程数
     */
    public static final int threads = 100;
    /**
     * 每个线程循环访问次数
     */
    public static final int loops = 100;

    public static void main(String[] args)
    {
        HttpSampleController controller = new HttpSampleController(URL, threads, loops);
        controller.start();
    }
}
