package com.geekbang.perfma4j;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

/**
 * Http一次采样结果
 */
@Data
public class HttpSampleResult implements Comparable<HttpSampleResult>
{
    /**
     * 是否成功
     */
    private boolean success;

    /**
     * 响应时间（单位：毫秒）
     */
    private Integer responseTime;

    @Override
    public int compareTo(@NotNull HttpSampleResult result)
    {
        return this.responseTime.compareTo(result.responseTime);
    }
}
