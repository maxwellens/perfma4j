package com.geekbang.perfma4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Http采样控制器
 */
public class HttpSampleController
{
    private CountDownLatch countDownLatch;
    private HttpSamplerListener listener;
    private List<HttpSampler> samplerList = new ArrayList<>();

    public HttpSampleController(String url, int threads, int loops)
    {
        countDownLatch = new CountDownLatch(threads);
        listener = new HttpSamplerListener(countDownLatch);
        for (int i = 0; i < threads; i++)
        {
            HttpSampler sampler = new HttpSampler(url, loops);
            sampler.addSamplerListener(listener);
            samplerList.add(sampler);
        }
    }

    public void start()
    {
        for (HttpSampler sampler : samplerList)
        {
            sampler.start();
        }
        try
        {
            countDownLatch.await();
            listener.printReport();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
